package com.attedance.sms;

import javax.swing.SwingUtilities;

import com.attedance.sms.ui.Menu;

public class AppSwingSMS {

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				new Menu();

			}

		});

	}

}
