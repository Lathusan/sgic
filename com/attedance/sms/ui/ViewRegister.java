package com.attedance.sms.ui;

import javax.swing.JCheckBox;
import javax.swing.JLabel;

import com.attedance.sms.logic.Register;

public class ViewRegister extends LayoutFrame {

	private static final long serialVersionUID = 1L;
	JLabel lblDate, lbName, lbresult, lbstudnam;
	Register register;

	public ViewRegister(Register register) {
		super("View Regiester");
		this.register = register;
		displayNames();
		displayDates();
		displayAttendence();

	}

	public void displayNames() {
		int top = 40;
		for (int i = 0; i < this.register.student.length; i++) {
			JLabel lbstudnam = new JLabel();
			lbstudnam.setText(this.register.student[i]);
			lbstudnam.setBounds(10, top, 50, 30);
			this.add(lbstudnam);
			top = top + 50;
		}
	}

	public void displayDates() {

		int left = 90;
		for (int i = 0; i < this.register.dateConter; i++) {
			JLabel lbDate = new JLabel();
			lbDate.setText(this.register.date[i]);
			lbDate.setBounds(left, 10, 80, 30);
			this.add(lbDate);
			left = left + 90;
		}
	}

	public void displayAttendence() {
		int top = 40;
		int left = 90;
		for (int i = 0; i < this.register.student.length; i++) {
			left = 90;
			for (int j = 0; j < this.register.dateConter; j++) {
				JLabel lbresult = new JLabel();
				lbresult.setText(Boolean.toString(this.register.register[i][j]));
				lbresult.setBounds(left, top, 70, 30);
				this.add(lbresult);
				left = left + 90;
			}
			top = top + 50;
		}
	}

}
