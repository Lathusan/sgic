package com.attedance.sms.logic;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Register extends JFrame {

	private static final long serialVersionUID = 1L;

	JFrame jf;
	JLabel jl1, jl2, jl3;

	public String student[] = { "Ajith", "Bala", "Danniel", "Jana", "Kumar" };
	public String date[] = new String[7];
	public int dateConter = 0;
	public boolean register[][] = new boolean[student.length][7];

	public void markregister(String dateStr, boolean[] arr) {

		date[dateConter] = dateStr;

		for (int i = 0; i < student.length; i++) {

			register[i][dateConter] = arr[i];
		}
		dateConter++;
	}

	public void printRegister() {

		jf = new JFrame("Print Register");
		this.setLayout(null);
		this.setSize(300, 400);

		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int h = dimension.height;
		int w = dimension.width;

		int fh = this.getSize().height;
		int fw = this.getSize().width;

		int y = (h - fh) / 2;
		int x = (w - fw) / 2;

		this.setLocation(x, y);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

	/*	int set = 20;
		for (int i = 0; i < student.length; i++) {
			jl1.setText(student[i]);
			jl1.setBounds(10, set, 50, 10);
			set = set + 20;
			jf.add(jl1);
		}*/

	//	System.out.print("\t");
		for (int i = 0; i < dateConter; i++) {
			jl1.setText(date[i] + "|\t");
			jl1.setBounds(20,20,20,20);
			System.out.print(date[i] + "|\t");
		}
		System.out.println();
		for (int i = 0; i < student.length; i++) {
			System.out.print(student[i] + "\t");
			for (int j = 0; j < dateConter; j++) {
				jl2.setText("   " + register[i][j] + "    \t");
				jl2.setBounds(20,20,20,20);
				System.out.print("   " + register[i][j] + "    \t");
			}
			jl3.setText("");
			System.out.println();
		}
	}

}
